package models

import (
	"errors"
	"github.com/blang/semver/v4"
)

func Version(val interface{}) error {
	v, ok := val.(string)
	if !ok {
		return errors.New("the type of version must be string")

	}
	_, err := semver.Parse(v)
	if err != nil {
		return err
	}
	return nil
}
