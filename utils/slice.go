package utils

type FilterFunc func(index int, item string) bool

func Filter(s []string, callback FilterFunc) []string {
	l := 0
	for i, val := range s {
		if callback(i, val) {
			s[l] = val
			l++
		}
	}
	return s[:l]
}
