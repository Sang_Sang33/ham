package utils

import (
	"fmt"
	"os"
	"os/user"
	"path/filepath"
)

func Exists(path string) bool {
	_, err := os.Stat(path)
	if err != nil {
		return os.IsExist(err)
	}
	return true
}

func GlobalRepo() string {
	current, err := user.Current()
	if err != nil {
		fmt.Println(err)
	}
	return filepath.Join(current.HomeDir, ".ham")
}

func HamrcDir() string {
	return filepath.Join(GlobalRepo(), "hamrc")
}
