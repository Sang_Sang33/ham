package main

import (
	"hpmGo/utils"
	"os"
	"path/filepath"
	"testing"
)

func TestCopyDir(t *testing.T) {
	t.Run("recursive cp -r source to dest", func(t *testing.T) {
		cwd, _ := os.Getwd()
		sourcePath := filepath.Join(cwd, "dist")
		destPath := filepath.Join(cwd, "dest")
		err := utils.CopyDir(sourcePath, destPath)
		if err != nil {
			t.Errorf("copy dir failed")
		}
	})
}
