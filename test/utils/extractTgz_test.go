package main

import (
	"hpmGo/utils"
	"os"
	"path/filepath"
	"testing"
)

func TestExtract(t *testing.T) {
	t.Run("uncompress tar to cwd", func(t *testing.T) {
		cwd, _ := os.Getwd()
		dest := filepath.Join(cwd, "dist")
		os.RemoveAll(dest)
		os.Mkdir(dest, os.ModeDir)
		source := filepath.Join("./default.tgz")
		f, err := os.Open(source)
		if err != nil {
			t.Log(err)
		}
		utils.Extract(f, dest)
		src := filepath.Join(dest, "src")
		headers := filepath.Join(dest, "headers")
		bundleJson := filepath.Join(dest, "bundle.json")
		license := filepath.Join(dest, "lICENSE")
		readme := filepath.Join(dest, "README.md")
		makefile := filepath.Join(dest, "Makefile")
		if !(utils.Exists(src) && utils.Exists(headers) && utils.Exists(bundleJson) && utils.Exists(license) && utils.Exists(readme) && utils.Exists(makefile)) {
			t.Error("extract file failed")
		}
	})
}
