package main

import (
	"hpmGo/utils"
	"testing"
)

func TestFilter(t *testing.T) {
	t.Run("filter the slice return new slice", func(t *testing.T) {
		s := []string{"A", "a", "c"}
		s = utils.Filter(s, func(index int, item string) bool {
			return item != "A"
		})
		if len(s) != 2 {
			t.Error("filter the slice failed")
		}
	})
}
