package main

import (
	"hpmGo/core"
	"testing"
)

func TestManifest(t *testing.T) {
	t.Run("we could get targeted manifest from it with name and version", func(t *testing.T) {
		repo := core.Repository{}
		m, _ := repo.Remote.Manifest("@ohos/cjson", "3.0.0")
		if m.Name != "cjson" && m.Url == "" {
			t.Error("resolve manifest from remote repo failed")
		}
	})
	t.Run("we could the latest manifest if the version is *", func(t *testing.T) {
		repo := core.Repository{}
		m, _ := repo.Remote.Manifest("@ohos/cjson", "*")
		if m.Version != "3.0.1" && m.Url == "" {
			t.Error("resolve manifest from remote repo failed")
		}
	})
	t.Run("we could resolve manifest from the local path", func(t *testing.T) {
		repo := core.Repository{}
		m, _ := repo.Local.Manifest("@ohos/utils_native_lite", "3.0.0")
		if m.Name != "@ohos/utils_native_lite" && m.Version != "3.0.0" {
			t.Error("resolve manifest from local repo failed")
		}
	})
	t.Run("repo could resolve manifest from local and remote", func(t *testing.T) {
		repo := core.Repository{}
		bundlePath := "."
		bundle, _ := core.FromDir(bundlePath)
		manifests, _ := repo.Resolve(bundle)
		if len(manifests) != 2 {
			t.Error("resolve manifest from repo failed")
		}
	})
}
