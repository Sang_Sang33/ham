package main

import (
	"hpmGo/constants"
	"hpmGo/core"
	"hpmGo/utils"
	"path/filepath"
	"testing"
)

func TestResolve(t *testing.T) {
	t.Run("bundle could resolve dependencies from remote repository to manifests", func(t *testing.T) {
		bundlePath := "."
		var repo core.RemoteRepository
		bundle, _ := core.FromDir(bundlePath)
		manifests, err := bundle.Resolve(repo)
		if len(manifests) <= 0 && manifests[0].Url == "" {
			t.Errorf("resolve dependencies from bundle failed: %v", err)
		}
	})
}

func TestInstall(t *testing.T) {
	t.Run("Bundle could install manifest to ohos_bundles.", func(t *testing.T) {
		bundlePath := "."
		var repo core.RemoteRepository
		bundle, _ := core.FromDir(bundlePath)
		bundle.Install(repo)
		bundleJsonPath := filepath.Join(bundlePath, constants.OHOS_BUNDLES, "@ohos", "utils_native_lite", "bundle.json")
		if !utils.Exists(bundleJsonPath) {
			t.Error("bundle install failed")
		}
	})
}

func TestFromDir(t *testing.T) {
	t.Run("generate a bundle in the path", func(t *testing.T) {
		bundlePath := "."
		bundle, _ := core.FromDir(bundlePath)
		if bundle.Manifest.Name != "cjson" {
			t.Errorf("generate bundle failed in %s", bundlePath)
		}
	})
	t.Run("bundle could update dependencies", func(t *testing.T) {
		bundle, _ := core.FromDir(".")
		params := []string{"cjson@3.0.1"}
		bundle.UpdateDependencies(params)
		value, ok := bundle.Dependencies["cjson"]
		if !ok && value != "3.0.1" {
			t.Error("bundle update dependencies failed")
		}
	})
	t.Run("bundle could delete dependencies", func(t *testing.T) {
		bundle, _ := core.FromDir(".")
		params := []string{"test"}
		bundle.DeleteDependencies(params)
		_, ok := bundle.Dependencies["test"]
		if ok {
			t.Errorf("bundle delete dependencies %s failed", params[0])
		}
	})
}
