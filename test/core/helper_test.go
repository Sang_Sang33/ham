package main

import (
	"hpmGo/core"
	"testing"
)

func TestParseScopeName(t *testing.T) {
	t.Run("parse scopeName to scope and name", func(t *testing.T) {
		scopeName := "@ohos/cjson"
		scope, name := core.ParseScopeName(scopeName)
		if name != "cjson" && scope != "@ohos" {
			t.Errorf("parse scopeName %s failed", scopeName)
		}
	})
	t.Run("parse when scope is not exist", func(t *testing.T) {
		scopeName := "cjson"
		scope, name := core.ParseScopeName(scopeName)
		if name != "cjson" && scope != "" {
			t.Errorf("parse scopeName %s failed", scopeName)
		}
	})
}

func TestParseArg(t *testing.T) {
	t.Run("parse args to name and version", func(t *testing.T) {
		arg := "@ohos/cjson@3.0.0"
		name, version := core.ParseArg(arg)
		if name != "@ohos/cjson" && version != "3.0.0" {
			t.Errorf("parse arg %s failed", arg)
		}
	})
}
