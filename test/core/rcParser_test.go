package main

import (
	"hpmGo/core"
	"hpmGo/utils"
	"os"
	"path/filepath"
	"testing"
)

var (
	tempRepo  = filepath.Join(os.TempDir(), ".ham")
	tempHamrc = filepath.Join(tempRepo, "hamrc")
	config    = map[string]string{
		"strictSsl":        "false",
		"cache-expiration": "2000",
		"proxy":            "www.proxy.com",
	}
)

func init() {
	if !utils.Exists(tempRepo) {
		err := os.Mkdir(tempRepo, os.ModePerm)
		if err != nil {
			panic(err)
		}
	}
}

func TestSaveMapToFile(t *testing.T) {
	t.Run("set map to temp repo", func(t *testing.T) {
		_, err := core.SaveMapToFile(config, tempHamrc)
		if err != nil {
			t.Errorf("SaveMapToFile failed: %v", err)
		}
		got := utils.Exists(tempHamrc)
		if !got {
			t.Error("new hamrc file in temp dir failed")
		}
	})
}

func TestFromPath(t *testing.T) {
	t.Run("new rcParser from the path", func(t *testing.T) {
		rc, _ := core.FromPath(tempHamrc)
		for key := range config {
			if config[key] != rc.Get(key) {
				t.Error("Config map missmatch to default config")
			}
		}
	})
}

func TestSet(t *testing.T) {
	t.Run("Set key value to rcParser", func(t *testing.T) {
		rc, _ := core.FromMap(config, tempHamrc)
		rc.Set("hello", "world")
		rc.Set("strictSsl", "true")
		if rc.Get("hello") != "world" || rc.Get("strictSsl") != "true" {
			t.Error("set key value to hamrc failed")
		}
	})
}

func TestGet(t *testing.T) {
	t.Run("Get key value From rcParser", func(t *testing.T) {
		rc, _ := core.FromMap(config, tempHamrc)
		want := "www.proxy.com"
		got := rc.Get("proxy")
		if want != got {
			t.Error("get key failed")
		}
	})
}

func TestDelete(t *testing.T) {
	t.Run("Delete key value From rcParser", func(t *testing.T) {
		rc, _ := core.FromMap(config, tempHamrc)
		rc.Delete("strictSsl")
		want := ""
		got := rc.Get("strictSsl")
		if want != got {
			t.Error("delete key failed")
		}
	})
}
