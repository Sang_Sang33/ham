package main

import (
	"fmt"
	"github.com/urfave/cli/v2"
	cmd "hpmGo/commands"
	"hpmGo/constants"
	"hpmGo/core"
	"hpmGo/utils"
	"log"
	"os"
)

func main() {
	app := &cli.App{
		Name:    "hpmGo",
		Version: "1.0.0",
		Usage:   "HAM package manager",
		Action: func(c *cli.Context) error {
			fmt.Println("Hello, hpmGo cli, use hpmGo --help! ")
			return nil
		},
	}
	app.Commands = cmd.Command
	err := app.Run(os.Args)
	if err != nil {
		log.Fatal(err)
	}
}

func init() {
	initConfig()
}

func initConfig() {
	globalRepo := utils.GlobalRepo()
	hamrc := utils.HamrcDir()
	if !utils.Exists(globalRepo) {
		err := os.Mkdir(globalRepo, os.ModePerm)
		if err != nil {
			panic(err)
		}
	}
	if !utils.Exists(hamrc) {
		core.SaveMapToFile(constants.DEFAULT_CONFIG, hamrc)
	}
}
