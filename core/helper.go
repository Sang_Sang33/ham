package core

import (
	"regexp"
	"strings"
)

func ParseArg(params string) (key, value string) {
	index := strings.LastIndex(params, "@")
	if index > 0 {
		key = params[:index]
		value = params[index+1:]
		return
	}
	key = params
	value = "*"
	return
}

func ParseScopeName(scopeName string) (string, string) {
	re, _ := regexp.Compile(`^(@[\w\W]+)/([\w\W]+)$`)
	slice := re.FindStringSubmatch(scopeName)
	if len(slice) == 3 {
		return slice[1], slice[2]
	}
	return "", scopeName
}
