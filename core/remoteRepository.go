package core

import (
	"fmt"
	"github.com/imroc/req"
	"github.com/pkg/errors"
)

type RemoteRepository struct {
}

func (remoteRepo RemoteRepository) Manifest(name, version string) (m Manifest, err error) {
	url := fmt.Sprintf("https://repo.harmonyos.com/hpm/registry/api/bundles/detail/%s", name)
	var res DetailRes
	r, err := req.Get(url)
	if err != nil {
		err = errors.Errorf("request failed: %v", err)
		return
	}
	err = r.ToJSON(&res)
	if err != nil {
		err = errors.Errorf("Tojson Failed: %v", err)
	}
	m, err = res.Match(version)
	return
}

func (remoteRepo RemoteRepository) Resolve(bundle Bundle) (manifests []Manifest, err error) {
	return
}
