package core

import "github.com/pkg/errors"

type Repository struct {
	Local  LocalRepository
	Remote RemoteRepository
}

func (repo Repository) Manifest(name, version string) (m Manifest, err error) {
	return
}

func (repo Repository) Resolve(bundle Bundle) (manifests []Manifest, err error) {
	for name := range bundle.Dependencies {
		var depManifest Manifest
		versionRange := bundle.Dependencies[name]
		depManifest, err = repo.Local.Manifest(name, versionRange)
		if err == nil && depManifest.Name != "" {
			manifests = append(manifests, depManifest)
			continue
		}
		depManifest, err = repo.Remote.Manifest(name, versionRange)
		if err != nil && depManifest.Name == "" {
			err := errors.Errorf("can't resolve %s - %v in repository", name, versionRange)
			panic(err)
		}
		manifests = append(manifests, depManifest)
	}
	return
}
