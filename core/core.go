package core

type Segment struct {
	DestPath string `json:"destPath"`
}

type Ohos struct {
	Os     string `json:"os"`
	Kernel string `json:"kernel"`
	Board  string `json:"board"`
}

type Scripts map[string]string

type Repo interface {
	Manifest(name, version string) (Manifest, error)
	Resolve(bundle Bundle) ([]Manifest, error)
}

type Detail struct {
	Name        string      `json:"name"`
	Version     string      `json:"version"`
	Rom         int         `json:"rom"`
	Ram         int         `json:"ram"`
	Download    Download    `json:"download"`
	PublishTime int64       `json:"publishTime"`
	Manifest    Manifest    `json:"manifest"`
	Creator     string      `json:"creator"`
	Readmes     []Readmes   `json:"readmes"`
	ChangeLogs  []ChangeLog `json:"changeLogs"`
	Status      string      `json:"status"`
	RepoType    string      `json:"repoType"`
	Org         string      `json:"org"`
}

type Download struct {
	Addr     string `json:"addr"`
	Checksum string `json:"checksum"`
	Count    int    `json:"count"`
}

type Readmes []struct {
	Language string `json:"language"`
	Url      string `json:"url"`
}

type ChangeLog struct {
	Language string `json:"language"`
	Url      string `json:"url"`
}
