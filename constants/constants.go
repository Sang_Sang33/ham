package constants

import (
	"github.com/AlecAivazis/survey/v2"
	"hpmGo/models"
	"hpmGo/utils"
)

var (
	DEFAULT_CONFIG = map[string]string{
		"globalRepo":       utils.GlobalRepo(),
		"strictSsl":        "false",
		"cache-expiration": "2000",
		"proxy":            "www.proxy.com",
	}
	INIT_QUESTIONS = []*survey.Question{
		{
			Name:     "name",
			Prompt:   &survey.Input{Message: "bundle name: "},
			Validate: survey.Required,
		},
		{
			Name:     "version",
			Prompt:   &survey.Input{Message: "version: ", Default: "1.0.0"},
			Validate: models.Version,
		},
		{
			Name: "publishAs",
			Prompt: &survey.Select{
				Message: "publish as (default: code-segment): ",
				Options: []string{"binary", "code-segment", "distribution", "model", "plugin", "source", "template"},
				Default: "code-segment",
			},
		},
		{
			Name:     "license",
			Prompt:   &survey.Input{Message: "license(eg: Apache V2 | ISC | BSD | MIT | ...): ", Default: "MIT"},
			Validate: survey.Required,
		},
		{
			Name:     "description",
			Prompt:   &survey.Input{Message: "description: "},
			Validate: survey.Required,
		},
		{
			Name:     "private",
			Prompt:   &survey.Confirm{Message: "Is it private?  ", Default: false},
			Validate: survey.Required,
		},
	}
	OHOS_BUNDLES = "ohos_bundles"
)
