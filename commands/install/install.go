package install

import (
	"fmt"
	"github.com/urfave/cli/v2"
	"hpmGo/core"
	"os"
)

func Action(c *cli.Context) error {
	fmt.Println("hpmGo install")
	cwd, _ := os.Getwd()
	bundle, err := core.FromDir(cwd)
	bundle.UpdateDependencies(c.Args().Slice())
	repo := core.Repository{}
	err = bundle.Install(repo)
	if err != nil {
		return err
	}
	err = bundle.SaveManifestToJson()
	if err != nil {
		return err
	}
	return nil
}
