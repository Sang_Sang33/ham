package cmd

import (
	"github.com/urfave/cli/v2"
	"hpmGo/commands/config"
	"hpmGo/commands/initialize"
	"hpmGo/commands/install"
)

var Command = []*cli.Command{
	{
		Name:   "config",
		Usage:  "Global configuration list",
		Action: config.Action,
		Subcommands: []*cli.Command{
			{
				Name:   "set",
				Usage:  "set hamrc key to value",
				Action: config.SetAction,
			},
			{
				Name:   "delete",
				Usage:  "delete hamrc key to value",
				Action: config.DelAction,
			},
		},
	},
	{
		Name:   "init",
		Usage:  "Init project to cwd",
		Action: initialize.Action,
		Flags: []cli.Flag{
			&cli.StringFlag{
				Name:    "template",
				Aliases: []string{"t"},
				Usage:   "init project from template",
			},
		},
	},
	{
		Name:   "install",
		Usage:  "install bundle to cwd",
		Action: install.Action,
	},
}
