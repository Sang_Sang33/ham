package config

import (
	"errors"
	"github.com/urfave/cli/v2"
	"hpmGo/core"
	"hpmGo/utils"
)

func Action(c *cli.Context) error {
	rc, err := core.FromPath(utils.HamrcDir())
	if err != nil {
		return err
	}
	rc.ToJson()
	return nil
}

func SetAction(c *cli.Context) error {
	rc, err := core.FromPath(utils.HamrcDir())
	if err != nil {
		return err
	}
	if c.Args().Len()%2 != 0 {
		return errors.New("confirm key value is all exist")
	}
	for i := 0; i < c.NArg()/2; i++ {
		rc.Set(c.Args().Get(i), c.Args().Get(i+1))
	}
	err = rc.SaveToFile()
	if err != nil {
		return err
	}
	return nil
}

func DelAction(c *cli.Context) error {
	rc, err := core.FromPath(utils.HamrcDir())
	if err != nil {
		return err
	}
	for i := 0; i < c.NArg(); i++ {
		rc.Delete(c.Args().Get(i))
	}
	err = rc.SaveToFile()
	if err != nil {
		return err
	}
	return nil
}
